#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'jean-philippe_hautin'

import time
import sys
import memcache
import collections
import zlib
import gzip

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO


def readConfiguration(configFilename):
    try:
        configFile=open(configFilename)
        configuration={}
        for line in configFile:
            (key,value) = line.split('=')
            configuration[key]=value.strip()
        return configuration
    except IOError, ioe:
        raise IOError(ioe.errno,"configuration file not found '%s'" % configFilename)

def getSize(value):
    elts = value.split(' ')
    return int(elts[0][1:])
 
def getExpirationDate(value):
    elts = value.split(' ')
    return elts[2]

if __name__ == "__main__":

    DEFAULT_CONFIG_FILENAME = "servers.properties"

    keys={}

    if len(sys.argv) < 2:
        sys.stderr.write('Usage: '+sys.argv[0]+" <configuration file> <key to look at>\n")
        sys.exit(-1)
    elif len(sys.argv) < 3:
        sys.stderr.write('Usage: '+sys.argv[0]+" <configuration file> <key to look at>\n")
        print "using default configuration file %s" % DEFAULT_CONFIG_FILENAME
        configurationFilename=DEFAULT_CONFIG_FILENAME
        searchedKey=sys.argv[1]
    else:
        configurationFilename=sys.argv[1]
        searchedKey=sys.argv[2]
    slabsStats = None
    try : 
        configuration=readConfiguration(configurationFilename)
        mc1 = memcache.Client(['%s:%s' % (configuration['node1.ip'],configuration['node1.port'])], debug=1)
        mc1.delete(searchedKey)
    except IOError, ioe:
        sys.stderr.write("ERROR-%s : %s\n" % ( ioe.errno,ioe.strerror) )
        if slabsStats!=None:
            print "%r" % slabsStats
        print "\nKO"
        sys.exit(ioe.errno)
