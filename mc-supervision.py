#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'jean-philippe_hautin'

import time
import sys
import memcache
import collections

def calculateMissesPercentile(stat):
  content=stat['get_misses']
  capacity=stat['cmd_get']
  return calculatePercentile(content,capacity)

def calculateHitsPercentile(stat):
  content=stat['get_hits']
  capacity=stat['cmd_get']
  return calculatePercentile(content,capacity)

def calculateCapacity(stat):
  content=stat['bytes']
  capacity=stat['limit_maxbytes']
  return calculatePercentile(content,capacity)

def calculatePercentile(content,capacity):
  if int(capacity) != 0:
    percentile=float(content)*100/float(capacity)
    result = "%3.2f%%" % percentile
    return result
  else:
    return "N/A"


def checkConsistency(_stats1):
    if len(_stats1)>0:
    	(server1,stats1)=_stats1[0]
    print "                     ideal   |   %24s  |" % (server1)
    print "-----------------------------------------------------------|"
    print " version          : --       |  %24s   |" % (stats1['version'].strip())
    print " repcache version : --       |  %24s   |" % (stats1['repcached_version'].strip())
    print " curr_items       : --       |   %24s  |" % (stats1['curr_items']) 
    print " set              : --       |   %24s  |" % (stats1['cmd_set'])
    print " get              : --       |   %24s  |" % (stats1['cmd_get'])
    print " hits             : 100%%     |   %24s  |" % (calculateHitsPercentile(stats1))
    print " misses           :   0%%     |   %24s  |" % (calculateMissesPercentile(stats1))
    print " evictions        :    0     |   %24s  |" % (stats1['evictions'])    
    print " curr_conn        : <1024    |   %24s  |" % (stats1['curr_connections'])
    print " max_conn_reached :    0     |   %24s  |" % (stats1['listen_disabled_num'])
    print " capacity         : <90%%     |   %24s  |" % (calculateCapacity(stats1))


def readConfiguration(configFilename):
    try:
        configFile=open(configFilename)
        configuration={}
        for line in configFile:
            (key,value) = line.split('=')
            configuration[key]=value.strip()
        return configuration
    except IOError, ioe:
        raise IOError(ioe.errno,"configuration file not found '%s'" % configFilename)

if __name__ == "__main__":

    DEFAULT_CONFIG_FILENAME = "servers.properties"

    if len(sys.argv) < 2:
        sys.stderr.write('Usage: '+sys.argv[0]+" <configuration file>\n")
        print "using default configuration file %s" % DEFAULT_CONFIG_FILENAME
        configurationFilename=DEFAULT_CONFIG_FILENAME
    else:
        configurationFilename=sys.argv[1]
    stats1 = None
    stats2 = None
    try :
        configuration=readConfiguration(configurationFilename)
        mc1 = memcache.Client(['%s:%s' % (configuration['node1.ip'],configuration['node1.port'])], debug=1)
        while 1:
	        stats1 = mc1.get_stats()
	        checkConsistency(stats1)
                print stats1
        	print '\nOK'
		time.sleep(1)
    except IOError, ioe:
        sys.stderr.write("ERROR-%s : %s\n" % ( ioe.errno,ioe.strerror) )
        if stats1!=None:
            print "%r" % stats1
            print "%r" % stats2
        print "\nKO"
        sys.exit(ioe.errno)

