#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'jean-philippe_hautin'

import time
import datetime
import sys
import memcache
import collections

partitionNames={'1':'rei','2':'rea','4':'rel','5':'rec'}
users={'1':'reitecheuronet','2':'reatecheuronet','4':'vsuktecheurone','5':'vsectecheurone'}

PARTITION=0
CURRENCY=1
ORIGIN=2
DESTINATION=3
TRAVELDATE=4
FAMILYID=5

DATE_INPUT_FORMAT='%Y-%m-%d'
DATE_KEY_FORMAT='%m/%d/%Y'
DATE_OUTPUT_FORMAT='%d/%m/%Y'

if hasattr(datetime, 'strptime'):
    #python 2.6+
    strptime = datetime.strptime
else:
    #python 2.4 equivalent
    strptime = lambda date_string, format: datetime.datetime(*(time.strptime(date_string, format)[0:6]))

def readConfiguration(configFilename):
    try:
        configFile=open(configFilename)
        configuration={}
        for line in configFile:
            (key,value) = line.split('=')
            configuration[key]=value.strip()
        return configuration
    except IOError, ioe:
        raise IOError(ioe.errno,"configuration file not found '%s'" % configFilename)

def getSize(value):
    elts = value.split(' ')
    return int(elts[0][1:])
 
def getExpirationDate(value):
    elts = value.split(' ')
    expirationTime= int(elts[2])
    result = time.localtime(expirationTime)
    return result 

#userName;currencyId;origin;destination;startDateToRefresh;endDateToRefresh;familyId
##27GBSPXFRLPD300051-03/24/2014
def getKeyAsElts(value):
    partition=value[0]
    currencyId=value[1]
    origin=value[2:7]
    destination=value[7:12]
    familyId=value[12:18]
    travelDate=strptime(value[19:],DATE_KEY_FORMAT)
    return [ partition,currencyId,origin,destination,travelDate,familyId ]

def getStartDate(input):
    dt=strptime(input,DATE_INPUT_FORMAT)
    return dt

def getEndDate(input):
    dt=strptime(input,DATE_INPUT_FORMAT)
    dt = dt + datetime.timedelta(1)
    return dt

if __name__ == "__main__":

    DEFAULT_CONFIG_FILENAME = "servers.properties"

    keys={}

    if len(sys.argv) < 6:
        sys.stderr.write('Usage: '+sys.argv[0]+" <configuration file> <partition> <familyId> <startDate> <endDate>\n")
        sys.stderr.write("                           partitions : several values in ('rei',rea','rec','rel') separated by comma : example : rel,rec\n")
        sys.stderr.write("                           familyIds  : example : 300003,300051\n")
        sys.stderr.write("                           startDate  : example : 2014-12-03\n")
        sys.stderr.write("                           endDate    : example : 2014-12-25\n")
        print "using default configuration file %s\n" % DEFAULT_CONFIG_FILENAME
        configurationFilename=DEFAULT_CONFIG_FILENAME
        print "\nKO"
        sys.exit(-1)
    else:
        configurationFilename=sys.argv[1]
        partitions=sys.argv[2].split(',')
        familyIds=sys.argv[3].split(',')
        startDate=getStartDate(sys.argv[4])
        endDate=getEndDate(sys.argv[5])
        sys.stderr.write('Running with :\n\tpartitions=%s\n\tfamilyIds=%s\n\tstartDate=%s\n\tendDate=%s\n' % (partitions,familyIds,startDate,endDate))
    slabsStats = None
    try : 
        configuration=readConfiguration(configurationFilename)
        mc1 = memcache.Client(['%s:%s' % (configuration['node1.ip'],configuration['node1.port'])], debug=1)
        slabsStats = mc1.get_stats("items")
        for slabdesc in slabsStats[0][1] :
          #items:22:number:7
          if slabdesc.startswith('items:') and ':number' in slabdesc:
            slab = slabdesc.split(':')[1] 
            nbElts = slabsStats[0][1][slabdesc]
            request = "cachedump "+slab+" "+nbElts
            stats1 = mc1.get_stats(request)
            for key in stats1[0][1]:
              keys[key]=stats1[0][1][key]
        nbKeys = len(keys)
        if nbKeys==0:
          print 'cache is empty'
        else:
          print "userName;currencyId;origin;destination;startDateToRefresh;endDateToRefresh;familyId"
          for key in keys:
            elts = getKeyAsElts(key)
            currentPartition = partitionNames[elts[PARTITION]]
            if currentPartition in partitions and elts[FAMILYID] in familyIds and startDate<=elts[TRAVELDATE] and elts[TRAVELDATE]<=endDate:
             print "%s;%s;%s;%s;%s;%s;%s" % (users.get(elts[PARTITION],'unknown'),elts[CURRENCY],elts[ORIGIN],elts[DESTINATION],elts[TRAVELDATE].strftime(DATE_OUTPUT_FORMAT),elts[TRAVELDATE].strftime(DATE_OUTPUT_FORMAT),elts[FAMILYID])
    except IOError, ioe:
        sys.stderr.write("ERROR-%s : %s\n" % ( ioe.errno,ioe.strerror) )
        if slabsStats!=None:
            print "%r" % slabsStats
        print "\nKO"
        sys.exit(ioe.errno)
