#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'jean-philippe_hautin'

import time
import sys
import memcache
import collections

def calculateMissesPercentile(stat):
  content=stat['get_misses']
  capacity=stat['cmd_get']
  return calculatePercentile(content,capacity)

def calculateHitsPercentile(stat):
  content=stat['get_hits']
  capacity=stat['cmd_get']
  return calculatePercentile(content,capacity)


def calculateCapacity(stat):
  content=stat['bytes']
  capacity=stat['limit_maxbytes']
  return calculatePercentile(content,capacity)

def calculatePercentile(content,capacity):
  if int(capacity) !=0:
    percentile=float(content)*100/float(capacity)
    if percentile != 0:
  	result = "%3.2f%%" % percentile
    else:
	result='N/A'
  else:
    result='N/A'
  return result


def checkConsistency(_stats1,_stats2):
    if len(_stats1)>0:
    	(server1,stats1)=_stats1[0]
    else:
	stats1=None
    if len(_stats2)>0:
    	(server2,stats2)=_stats2[0]
    else:
	stats2=None
        server2=''
    
    if stats2 is not None and  ( stats1['version']!=stats2['version'] or stats1['repcached_version']!=stats2['repcached_version'] ):
        print "WARNING : version of servers are not the same."

    if stats2 is not None and stats1['limit_maxbytes']!=stats2['limit_maxbytes']:
        print "WARNING : max element size is not consistent."
    else:
        print "limit_maxbytes : %sMb " % (int(stats1['limit_maxbytes'])/(1024*1024))

    if stats2 is not None and stats1['total_items']!=stats2['total_items']:
        sys.stderr.write("%s : %s \n" % (server1,stats1['total_items']))
        sys.stderr.write("%s : %s \n" % (server2,stats2['total_items']))
        #raise IOError(12,"replication is not consistent on total items.")
    else:
        print "total_items    : %s " % stats1['total_items']

    print "                     ideal   |   %24s  |   %24s   |" % (server1,server2)
    print "------------------------------------------------------------------------------------------|"
    print " version          : --       |  %24s  |   %24s   |" % (stats1['version'],stats2['version'])
    print " repcache version : --       |  %24s  |   %24s   |" % (stats1['repcached_version'],stats2['repcached_version'])
    print " curr_items       : --       |  %24s  |   %24s   |" % (stats1['curr_items'],stats2['curr_items']) 
    print " set              : --       |  %24s  |   %24s   |" % (stats1['cmd_set'],stats2['cmd_set'])
    print " get              : --       |  %24s  |   %24s   |" % (stats1['cmd_get'],stats2['cmd_get'])
    print " hits             : 100%%     |  %24s  |   %24s   |" % (calculateHitsPercentile(stats1),calculateHitsPercentile(stats2))
    print " misses           :   0%%     |  %24s  |   %24s   |" % (calculateMissesPercentile(stats1),calculateMissesPercentile(stats2))
    print " evictions        : 0        |  %24s  |   %24s   |" % (stats1['evictions'],stats2['evictions'])    
    print " curr_conn        : <1024    |  %24s  |   %24s   |" % (stats1['curr_connections'],stats2['curr_connections'])
    print " max_conn_reached : 0        |  %24s  |   %24s   |" % (stats1['listen_disabled_num'],stats2['listen_disabled_num'])
    print " capacity         : <90%%     |  %24s  |   %24s   |" % (calculateCapacity(stats1),calculateCapacity(stats2))


def readConfiguration(configFilename):
    try:
        configFile=open(configFilename)
        configuration={}
        for line in configFile:
            (key,value) = line.split('=')
            configuration[key]=value.strip()
        return configuration
    except IOError, ioe:
        raise IOError(ioe.errno,"configuration file not found '%s'" % configFilename)

if __name__ == "__main__":

    DEFAULT_CONFIG_FILENAME = "servers.properties"

    if len(sys.argv) < 2:
        sys.stderr.write('Usage: '+sys.argv[0]+" <configuration file>\n")
        print "using default configuration file %s" % DEFAULT_CONFIG_FILENAME
        configurationFilename=DEFAULT_CONFIG_FILENAME
    else:
        configurationFilename=sys.argv[1]
    stats1 = None
    stats2 = None
    try :
        configuration=readConfiguration(configurationFilename)
        mc1 = memcache.Client(['%s:%s' % (configuration['node1.ip'],configuration['node1.port'])], debug=1)
        mc2 = memcache.Client(['%s:%s' % (configuration['node2.ip'],configuration['node2.port'])], debug=1)
        while 1:
	        stats1 = mc1.get_stats()
        	stats2 = mc2.get_stats()
	        checkConsistency(stats1,stats2)
        	print '\nOK'
		time.sleep(1)
    except IOError, ioe:
        sys.stderr.write("ERROR-%s : %s\n" % ( ioe.errno,ioe.strerror) )
        if stats1!=None:
            print "%r" % stats1
            print "%r" % stats2
        print "\nKO"
        sys.exit(ioe.errno)

