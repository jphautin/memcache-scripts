#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'jean-philippe_hautin'

import time
import sys
import memcache
import collections
import gzip
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO


def readConfiguration(configFilename):
    try:
        configFile=open(configFilename)
        configuration={}
        for line in configFile:
            (key,value) = line.split('=')
            configuration[key]=value.strip()
        return configuration
    except IOError, ioe:
        raise IOError(ioe.errno,"configuration file not found '%s'" % configFilename)

def getSize(value):
    elts = value.split(' ')
    return int(elts[0][1:])
 
def getExpirationDate(value):
    elts = value.split(' ')
    expirationTime= int(elts[2])
    result = time.localtime(expirationTime)
    return time.strftime("%Y-%m-%d %H:%M:%S",result)

def getValue(mc,searchedKey):
    print searchedKey
    value = mc.get(searchedKey)
    if value is not None :
      if value.startswith("{"):
        print value
        print "data is not compressed"
      else:
        fileobj = StringIO(value)
        gzf = gzip.GzipFile('dummy-name', 'rb', 9, fileobj)
        value=gzf.read()
        print value
        print "data is compressed"
    else:
       print "no data for %s" % searchedKey


if __name__ == "__main__":

    DEFAULT_CONFIG_FILENAME = "servers.properties"

    keys={}

    if len(sys.argv) < 2:
        sys.stderr.write('Usage: '+sys.argv[0]+" <configuration file>\n")
        print "using default configuration file %s" % DEFAULT_CONFIG_FILENAME
        configurationFilename=DEFAULT_CONFIG_FILENAME
    else:
        configurationFilename=sys.argv[1]
    slabsStats = None
    try : 
        configuration=readConfiguration(configurationFilename)
        mc1 = memcache.Client(['%s:%s' % (configuration['node1.ip'],configuration['node1.port'])], debug=1)
        slabsStats = mc1.get_stats("items")
        for slabdesc in slabsStats[0][1] :
          #items:22:number:7
          if slabdesc.startswith('items:') and ':number' in slabdesc:
            slab = slabdesc.split(':')[1] 
            nbElts = slabsStats[0][1][slabdesc]
            request = "cachedump "+slab+" "+nbElts
            print "running %s " % request
            stats1 = mc1.get_stats(request)
            for key in stats1[0][1]:
              keys[key]=stats1[0][1][key]
        nbKeys = len(keys)
        if nbKeys==0:
          print 'cache is empty'
        else:
          sumSizes = 0
          minSize = 999999999999999999
          maxSize = 0
          for key in keys:
            eltSize = getSize(keys[key])
            sumSizes = sumSizes + eltSize
            maxSize = max(eltSize,maxSize)
            minSize = min(minSize,eltSize)
          print "statistics : \n\tnb  : %s\n\tmin : %s\n\tavg : %s\n\tmax : %s\n" % (nbKeys,minSize,sumSizes/nbKeys,maxSize)
    except IOError, ioe:
        sys.stderr.write("ERROR-%s : %s\n" % ( ioe.errno,ioe.strerror) )
        if slabsStats!=None:
            print "%r" % slabsStats
        print "\nKO"
        sys.exit(ioe.errno)
